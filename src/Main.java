import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int num;
        boolean is_input_valid = true;
        try{
            System.out.println("Input an integer whose factorial will be computed");
            num = in.nextInt();
        }catch(Exception e){
            is_input_valid = false;
            num = 0;
            System.out.println("Invalid Input");
            e.printStackTrace();
        }

        if(!is_input_valid) return;

        int answer = 1;
        if(num != 0){
            int counter = 1;
            while(counter <= Math.abs(num)){
                answer *= counter;
                counter++;
            }
        }

        int for_loop_answer = 1;
        for(int for_loop_counter = 1; for_loop_counter <= Math.abs(num); for_loop_counter++){
            for_loop_answer *= for_loop_counter;
        }

        if(answer == for_loop_answer){
            if(num < 0) answer *= -1;
            System.out.println("The factorial of " + num + " is " + answer);
        }
    }
}